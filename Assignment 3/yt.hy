;;; CS 581 Assignment 3
;;; YouTube Data Processing
;;; by Eric S. Londres
;;; (c) 2019

;; Initially adapted from youtube_data.py by Cheryl Dugas

;; import modules needed for later
(import [apiclient.discovery [build]] argparse csv unidecode)

(setv API_KEY "AIzaSyCBWm8rkP1MkMOn9DEh0jZcwypl0IEdiy0") ; my personal API key
(setv API_VERSION "v3")                                  ; which version of the youtube API to use


(defn youtube-search [search-term search-max-value]
  "Searches YouTube given specified parameters. The first parameter is the query to be searched for, and the second parameter is how many entries to return (must be less than 50)."
  ;; compile youtube object to prepare functions
  (setv youtube (build "youtube" API_VERSION :developerKey API_KEY))

  (setv most-viewed ["" 0])        ; will be the return value
  
  ;; search with the given terms 
  (setv search-response (.execute (.list (.search youtube) :q search-term :part "id,snippet" :maxResults search-max-value)))

  ;; create a CSV for video list and write the headings line
  (setv csvFile (open "video_results.csv" "w"))
  (setv csvWriter (.writer csv csvFile))
  (.writerow csvWriter ["TITLE" "ID" "VIEWS" "LIKES" "DISLIKES" "COMMENTS" "FAVORITES"]) ; print the header
  (for [search-result (.get search-response "items" [])] ; loop for each result from the query
    (if (= "youtube#video" (get search-result "id" "kind")) ; if the specific result we're looking at is a video
        (do                                                 ; needed for scoping to work correctly - compare to (progn)
          (setv video-id (get search-result "id" "videoId")) ; set the video ID, it's used immediately
          (setv video-response (.execute (.list (.videos youtube) :id video-id :part "statistics"))) ; gather data from the API about the video in question
          (setv title (.unidecode unidecode (get search-result "snippet" "title"))) ; convert the title from unicode into ascii
          (for [video-results (.get video-response "items" [])] ; loop for each item in the video data
            (setv view-count (get video-results "statistics" "viewCount")) ; viewCount is always present
            ;; (nonlocal most-viewed) ; allows setting the global variable 
            (if (> (int view-count) (last most-viewed)) (setv most-viewed [title (int view-count)])) ; if this is the most-viewed video, update the most-viewed variable
                
            (.writerow csvWriter [ ; call writerow to print a line in the CSV for each video found
                                  ;; the commas are automatically added by the [] operator
                                  title 
                                  video-id 
                                  view-count
                                  ;; for the next few, they might not exist so we need to have a fallback case of 0
                                  (if (in "likeCount" (get video-results "statistics"))
                                      (do (get video-results "statistics" "likeCount"))
                                      (do 0))
                                  (if (in "dislikeCount" (get video-results "statistics"))
                                      (do (get video-results "statistics" "dislikeCount"))
                                      (do 0))
                                  (if (in "commentCount" (get video-results "statistics"))
                                      (do (get video-results "statistics" "commentCount"))
                                      (do 0))
                                  (if (in "favoriteCount" (get video-results "statistics"))
                                      (do (get video-results "statistics" "favoriteCount"))
                                      (do 0))
                                  ])))))  
  (.close csvFile)
  most-viewed)

(defmain [&rest _]
  "This is the entry point of the file if the program is called as a script. Otherwise, you can just call (youtube-search) directly."
  (setv parser (argparse.ArgumentParser :description "YouTube Search"))
  (.add-argument parser "-s" :default "how to eat cereal") ; users will use the -s flag for searches
  (.add-argument parser "-m" :default 10)                  ; users will use the -m flag for the amount of results
  (setv args (parser.parse-args))
  (setv ret (youtube-search args.s args.m)) ; call the search function, and store the return value
  (print (+ "The most viewed video was " (first ret) " with a total of " (str (last ret)) " views.")) ; print out the stuff
  0)                             ; return code of normal status
