#include "parse_file.hpp"
#include <vector>
#include <string>
#include <boost/regex.hpp>

#define ITERATOR(a, b, c) (a, b, c, -1)

using std::string;
using std::vector;
using boost::regex;

typedef vector<string> csvRow;
typedef boost::cregex_token_iterator citer;
typedef boost::sregex_token_iterator siter;

const regex line("\\r\\n|\\n"); // Windows or Unix line endings
const regex token(","); // comma to seperate values

vector<csvRow> parse_file(const char * data) {
  vector<csvRow> ret;
  
  // use the boost token iterator to divide the CSV into lines
  // cregex_token_iterator means it uses const char*
  citer lines ITERATOR(data, data + strlen(data), line);
  citer cempty;

  while(lines != cempty) {
    string line = lines->str();
    ++lines;
 
    // Split the current line in tokens
    siter tokens ITERATOR(line.begin(), line.end(), token);
    siter sempty;
    csvRow row;
	
    while(tokens != sempty) {
      row.push_back(tokens->str());
      ++tokens;
    }
	          
    if(line.back() == ',')
      row.push_back("");
        	
    ret.push_back(row);
  }
   
  return ret;
}
