#pragma once
#ifndef PARSE_FILE
#define PARSE_FILE

#include <vector>
#include <string>

std::vector<std::vector<std::string>> parse_file(const char * data);

#endif //PARSE_FILE
