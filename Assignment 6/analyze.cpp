#include "analyze.hpp"

#include <unordered_map>
#include <math.h>
#include <iostream>

/** analyze.cpp
 * This file contains the logic for analyzing the data retreived from the file
 * First we iterate through the in-memory vectors representing the CSV lines
 * As we do so, we keep track of the simple counters while also building a 
     adjacency matrix of the graph.
 * The adjacency matrix allows for hyper-fast generation of the triangle data,
      which is implemented here as a breath-first search.
 * Breath-first search is O(N) on an adjacency matrix - it's far, far less
         efficient on the original unsorted/unprocessed adjacency list.
 * The value of the index of each position in the adjacency matrix is the sign
      of the edge.
 */

using std::vector;
using std::string;

typedef std::unordered_map<string, std::unordered_map<string, short>> adjMatrix;

statistics analyze(vector<vector<string>> data) {
  statistics stats;
  stats.edges = data.size();

  // This is the adjacency matrix used to represent the full graph
  // Unordered maps provide O(log n) efficiency for most operations, which is
  // essentially required for gigantic data sets such as this.
  // The final value of the map is the sign of the edge
  adjMatrix matrix;
  
  for(vector<string> edge : data) {
    /* Each line of the CSV is represented as one 
       entry of the toplevel vector. Specifically,
       each entry is a vector of strings where
       each string is one token of the CSV. */

    if(edge.at(0) == edge.at(1)) {
      // we are a self-loop
      ++stats.selfLoops;
    } else {
      if((unsigned)matrix[edge.at(0)][edge.at(1)] != 1) {
	// add the current edge to the adjacency matrix if it isn't there
	matrix[edge.at(0)][edge.at(1)] = 0;
      }
            
      // we are not a self-loop
      ++stats.triadicEdges;
      if(edge.at(2) == "1") {
	// we are a trusted, non-self edge
	++stats.trustEdges;
	matrix[edge.at(0)][edge.at(1)] = 1;	
      } else if(edge.at(2) == "-1") {
	// we are a distrusted, non-self edge
	++stats.distrustEdges;
	matrix[edge.at(0)][edge.at(1)] = -1;
      } else {
	// we are a mistake
	++stats.unprocessed;
      }
    }
  }
  
  // we've iterated over all the loops, now we need the auxiliary stats
  
  // trustProbability is the positive edges divided by the totedges
  stats.trustProbability = (float)stats.trustEdges / (float)stats.triadicEdges;
  stats.distrustProbability = 1.0 - stats.trustProbability;

  
  // print what we have so far so user gets feedback before the big loop
  std::cout << "Edges in network: " << stats.edges << std::endl
	    << "Self-loops: " << stats.selfLoops << std::endl
	    << "Triadic edges used: " << stats.triadicEdges << std::endl
	    << "Trust edges: " << stats.trustEdges << "   "
	    << "Probability p: " << stats.trustProbability << std::endl
	    << "Distrust edges: " << stats.distrustEdges << "   "
	    << "Probability 1-p: " << stats.distrustProbability << std::endl
	    << "Unprocessed (error) edges: " << stats.unprocessed << std::endl;
  

  // Now we need to calculate the number of triangles.
  // To do that, we need to generate all possible combinations of 3 nodes
  //   and check if there's an edge between all of them
  // Because we use a ragged-right map, we know off the bat that we won't even
  //   be able to access the final sign store if there is no cycle
  for(auto row = matrix.cbegin(); row != matrix.cend(); ++row) {
    for(auto col = row->second.cbegin(); col != row->second.cend(); ++col) {
      for(auto ri = matrix[col->first].cbegin(); ri != matrix[col->first].cend(); ++ri) {	
	/* We now have three values: row->first, col->first and row_iter->first
	 * We know that row->first => col->first is an edge by definition
	 * but we need to check that col->first => row_iter->first as well as
	   col->first => row_iter->first
	 * But we still need to check if row_iter->first => row->first is a row.
	 * if and only if it is, we have a triangle.
	 */

	// row_iter->first => row->first
	if(!(matrix[ri->first].contains(row->first)) || matrix[ri->first][row->first] == 0) {
	  // no edge exists; ri->first => row->first has failed
	  continue;
	}
	
	// If we made it here, there is a triangle!
	++stats.triangles;
	const bool ab = matrix[row->first][col->first] > 0;
	const bool ac = matrix[ri->first][row->first] > 0;
	const bool bc = matrix[col->first][ri->first] > 0;
	if(ab && ac && bc) {
	  ++stats.actualTTT;
	} else if((ab && ac) || (ab && bc) || (bc && ac)) {
	  ++stats.actualTTD;
	} else if(ab || ac || bc) {
	  ++stats.actualTDD;
	} else {
	  // none are true
	  ++stats.actualDDD;
	}
      }
    }
  }

  // each triangle will have been calculated three times, so we adjust the count
  stats.triangles /= 3;
  stats.actualDDD /= 3;
  stats.actualTDD /= 3;
  stats.actualTTD /= 3;
  stats.actualTTT /= 3;
   

  // The triangles have been computed so we calculate the expected distribution
  stats.expectedTTT = pow(stats.trustProbability, 3);
  stats.expectedTTD = pow(stats.trustProbability, 2) * stats.distrustProbability;
  stats.expectedTDD = stats.trustProbability * pow(stats.distrustProbability, 2);
  stats.expectedDDD = pow(stats.distrustProbability, 3);

  
  return stats;
}

