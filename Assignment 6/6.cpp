/**
 * CS 581 Assignment 6 - Network Analysis
 * by Eric S. Londres
 */
#include <iostream>
#include <vector>
#include <iomanip> // std::setw
#include "load_file.hpp"
#include "parse_file.hpp"
#include "analyze.hpp"

using std::string;

int main(int argc, char ** argv) {
	string filename;
	if(argc == 2) {
		filename = argv[1];
	} else if(argc == 1) {
		// input file name was not specified on the cli
		std::cout << "Enter input filename: ";
		std::cin >> filename;
	} else {
		// user gave too many cli options
		std::cout << "Usage: 6 [filename.csv]"; 
		return 1;
	}

	const char * file = load_file(filename);

	std::vector<std::vector<string>> data = parse_file(file);

	// retrieve the statistics
	statistics results = analyze(data);

	
	std::cout << "Triangles: " << results.triangles << std::endl
		  << "Expected Distribution: " << std::endl
		  << std::setw(16) << "Type" << std::setw(16) << "Percent"
		  << std::setw(16) << "Amount" << std::endl
		  << std::setw(16)<< "TTT"
		  << std::setw(16) << results.expectedTTT
		  << std::setw(16) << results.expectedTTT * results.triangles
		  << std::endl
		  << std::setw(16)<< "TTD"
		  << std::setw(16) << results.expectedTTD
		  << std::setw(16) << results.expectedTTD * results.triangles
		  << std::endl
		  << std::setw(16)<< "TDD"
		  << std::setw(16) << results.expectedTDD
		  << std::setw(16) << results.expectedTDD * results.triangles
		  << std::endl
		  << std::setw(16)<< "DDD"
		  << std::setw(16) << results.expectedDDD
		  << std::setw(16) << results.expectedDDD * results.triangles
		  << std::endl;
	  
	std::cout << "Actual Distribution: " << std::endl
		  << std::setw(16) << "Type" << std::setw(16) << "Percent"
		  << std::setw(16) << "Amount" << std::endl
		  << std::setw(16)<< "TTT"
		  << std::setw(16) << (float)results.actualTTT/(float)results.triangles
		  << std::setw(16) << results.actualTTT
		  << std::endl
		  << std::setw(16)<< "TTD"
		  << std::setw(16) << (float)results.actualTTD/(float)results.triangles
		  << std::setw(16) << results.actualTTD
		  << std::endl
		  << std::setw(16)<< "TDD"
		  << std::setw(16) << (float)results.actualTDD / (float)results.triangles
		  << std::setw(16) << results.actualTDD
		  << std::endl
		  << std::setw(16)<< "DDD"
		  << std::setw(16) << (float)results.actualDDD / (float)results.triangles
		  << std::setw(16) << results.actualDDD
		  << std::endl;
	
	return 0;    
}
