#pragma once
#ifndef LOAD_FILE
#define LOAD_FILE

#include <string>

// Given a filename,
// returns a const char* containing the contents of the file,
// via mmap

const char * load_file(std::string filename);

#endif // LOAD_FILE
