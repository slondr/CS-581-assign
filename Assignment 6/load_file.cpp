#include "load_file.hpp"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

const char * load_file(std::string filename) {
  size_t file_length;
  
  int fd = open(filename.data(), O_RDONLY);
  if(fd == -1) {
    perror("error opening file");
    exit(2);
  }
  
  struct stat s;
  if(fstat(fd, &s) == -1) {	
    perror("fstat failed on file descriptor.");
    exit(3);
  }

  file_length = s.st_size;

  // map the file
  const char * address = static_cast<const char *>(mmap(0, file_length, PROT_READ, MAP_PRIVATE, fd, 0));
  if(address == MAP_FAILED) {
    perror("mmap failed.");
    exit(4);
  }
    
  return address;
  
}
