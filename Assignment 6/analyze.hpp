#pragma once
#ifndef ANALYZE
#define ANALYZE

// headers are needed for type signature
#include <vector>
#include <string>

// data collection structure
struct statistics {
  unsigned edges = 0;
  unsigned selfLoops = 0;
  unsigned triadicEdges = 0; // edges - selfLoops
  unsigned trustEdges = 0; // don't include selfLoops
  unsigned distrustEdges = 0; // don't include selfLoops
  unsigned unprocessed = 0; // if the sign is invalid
  double trustProbability = 0.00;
  double distrustProbability = 0.00; // 1 - trustProbability
  unsigned triangles = 0;
  double expectedTTT = 0; // 3x Trust
  double expectedTTD = 0; // 2x Trust, 1x Distrust
  double expectedTDD = 0; // 1x Trust, 2x Distrust
  double expectedDDD = 0; // 3x Distrust
  double actualTTT = 0; // 3x Trust
  double actualTTD = 0; // 2x Trust, 1x Distrust
  double actualTDD = 0; // 1x Trust, 2x Distrust
  double actualDDD = 0; // 3x Distrust
};

// main function
statistics analyze(std::vector<std::vector<std::string>> data);

#endif
