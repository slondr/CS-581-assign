/// NINE \\\
package main
/* CS 581 Assignment 9
   by Eric S. Londres
Pulls data from the Twitter API and does some basic analysis of it.
To run the program, either compile it with go build or just run it with go run
Either pass command line flags or the program will prompt you for search terms
Example run: go run nine.go -search_term1=flowers -search_term2=oatmeal -results=50
Another example: go run nine.go --results 100
  (in this example, the program will prompt for both search terms.)
Command line flags can be of the form -[flag]=[value] or --[flag] [value]

Also, you will need to install both GitHub dependencies via go get beforehand

To view documentation for this program seperated from source code, just run godoc
*/



import (
	"fmt"
	"os"
	"flag"
	"bufio"
	"strconv"
	"net/url"
	"encoding/csv"
	"github.com/ChimeraCoder/anaconda" // used for Twitter API connection
	"github.com/cdipaolo/sentiment"    // used for sentiment analysis
)


// Accepts a uint8 sentiment value and returns a string indicating if that
// value representes positive, negative, or neutral sentiment.
func prettifySentiment(s uint8) string {
	switch s {
	case 1: return "Positive sentiment"
	case 0: return "Negative sentiment"
	default: return "Neutral/unknown sentiment"
	}
}

// Given a boolean value, converts that value into a human-readable string
// specifying if someone is verified or not.
func verified(v bool) string {
	isVerified := ""
	if !v {
		isVerified = "Not "
	}
	return fmt.Sprintf("%sVerified", isVerified)
}

// Accepts the number of positive sentiments from a total number of messages,
// returning a string representing whether the sentiment is positive/negative/neutral
// and the magnitude thereof
func checkSent(sentimentality, c int) string {
	// Given a sentimenality value and total source arity c, compute overall sentimentality of s/c
	count := float64(c)
	s := float64(sentimentality)

	// 'unanimously' means 100% positive/negative
	// 'overwhelmingly' means >75% positive/negative
	// no specifier means 50%-75% positive/negative
	if s == (count * 0.5) {
		return "perfectly neutral"
	} else if s == count {
		return "unanimously positive"
	} else if s > (count * 0.75) {
		return "overwhelmingly positive"
	} else if s > (count * 0.5) {
		return "positive"
	} else if s == 0.0 {
		return "unanimously negative"
	} else if s < (count * 0.25) {
		return "overwhelmingly negative"
	} else {
		return "negative"
	}
}

// Accepts a search term and number of desired results, both strings. Connects
// to the Twitter API, runs a search with the given parameters, generates slices
// with the results for later CSV parsing/printing, then prints to stdout the
// given search term, the number of actual results that Twitter returned, and
// a pretty-printed representation of the overall sentimentality of the given
// term based on the results.
func query(term, count string) [][]string {
	// format for each record is userid,name,followers,friends,verified?,RTs,text,sentiment
	var ret [][]string
	var sentimentality int
	
	model, error := sentiment.Restore()
	api := anaconda.NewTwitterApiWithCredentials(	"760545776212717568-cTbfeQcYnR1j7exoeh6sABAKn25SMn3",	"OUHlacNlcenXtWsGn12mXC1oL2fl2L4kxbm0g0AkRpqte",	"UK75CylP35Ok6TmMd0pZmecJF", "2Pko2zvqmPxaBbj81jtRSKhbGX4lQLBDQMV8wYENXpleokNA6M")
	// the next two lines are how Anaconda sets the number of results
	// NOTE: Twitter will only give you 100 max, even if you ask for more
	v := url.Values{}
	v.Set("count", count)
	result, _ := api.GetSearch(term, v)
	for _, tweet := range result.Statuses {
		var record []string
		record = append(record, strconv.FormatInt(tweet.User.Id, 10))
		record = append(record, tweet.User.Name)
		record = append(record, strconv.Itoa(tweet.User.FollowersCount))
		record = append(record, strconv.Itoa(tweet.User.FriendsCount))
		record = append(record, verified(tweet.User.Verified))
		record = append(record, strconv.Itoa(tweet.RetweetCount))
		record = append(record, tweet.Text)
		
		// sentiment analysis
		if error == nil {
			analysis := model.SentimentAnalysis(tweet.Text, sentiment.English)
			sentimentality += int(analysis.Score)
			record = append(record, prettifySentiment(analysis.Score))
		} else {
			fmt.Fprintf(os.Stderr, "Error: %s", error)
			record = append(record, "")
		}

		ret = append(ret, record)		
	}
	
	fmt.Printf("Search completed for %s returning %d results.\n", term, result.Metadata.Count)
	fmt.Printf("The overall sentimentality of the search term is %s.\n", checkSent(sentimentality, result.Metadata.Count))
	
	return ret
}

	
// The entry point to the program. Parses command line arguments, prompts for
// search terms if not provided, then writes the resulting data into two CSVs
// titled `search1.csv` and `search2.csv`
func main() {
	// parse the command line arguments
	firstTerm := flag.String("search_term1", "", "The first term to search")
	secondTerm := flag.String("search_term2", "", "The second term to search")
	count := flag.String("results", "1", "The maximum number of results to return")
	flag.Parse()

	// check to ensure we received both terms; if not, prompt again
	in := bufio.NewReader(os.Stdin)
	if *firstTerm == "" {
		fmt.Print("Enter a search term: ")
		*firstTerm, _ = in.ReadString('\n')
	}
	if *secondTerm == "" {
		fmt.Print("Enter another search term: ")
		*secondTerm, _ = in.ReadString('\n')
	}

	// Now we call query() with each term
	file1, err := os.Create("search1.csv") // truncates/overwrites file if extant
	if err != nil {
		panic(err)
	} 
	fileWriter := csv.NewWriter(file1)
	fileWriter.WriteAll(query(*firstTerm, *count))

	fmt.Println()
	
	file2, err := os.Create("search2.csv")
	if err != nil {
		panic(err)
	}
	csv.NewWriter(file2).WriteAll(query(*secondTerm, *count))

}
