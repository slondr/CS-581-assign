#+TITLE: CS 581 Assignment 4(1)
#+AUTHOR: Eric S. Londres
#+DATE: 2 October 2019
#+OPTIONS: toc:nil
#+latex_class_options: [12pt]


In the beginning of 2012, data scientists employed at Facebook altered what roughly 700,000 Facebook users experienced on their platform by deliberately showing users content with a certain mood, such as "happy" or "sad."

Facebook's terms of service allow the company to perform these experiments legally. However, analysts and experts have sounded alarms that the practice was unethical and philosophically disturbing.

One of the study's authors commented recently that ``[i]n hindsight, the research benefits of the paper may not have justified all this anxiety.'' They also claimed that Facebook's practices have ``come a long way'' since the study was run.

The study ultimately concluded that ``emotional contagion'' is a valid phenomenon. Users shown negative content produced more negative content, and users shown positiveq content produced more positive content. The study also foqund that the less emotionally charged content users were shown, the less content they produced overall.

A psychology expert has claimed that the methodology used by the study was flawed, as it gave illogical ratings to short-form messages that to a human has a clear positive/negative intent. Many other experts have claimed that Facebook failed to secure consent for this study, making it unethical, and also that the study failed to abide by even Facebook's own standards for ethicality.
